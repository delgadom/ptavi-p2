#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


def add(op1, op2):
    """ Function to sum the operands. Ops have to be ints """
    return op1 + op2


def sub(op1, op2):
    """ Function to substract the operands """
    return op1 - op2


def multiplicacion(op1, op2):
    """ Function to substract the operands """
    return op1 * op2

if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "+":
        result = add(operando1, operando2)
    elif sys.argv[2] == "-":
        result = sub(operando1, operando2)
    else:
        sys.exit('Operación sólo puede ser + o -')

    print(result)
