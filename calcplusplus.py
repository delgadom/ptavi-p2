#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import csv
import calcoohija

if __name__ == "__main__":
    with open(sys.argv[1]) as documento:
        fichero = csv.reader(documento)
        lineas = documento.readlines()
        for linea in lineas:
            lista = linea.split(',')
            numeros = list(map(int, lista[1:]))

            if lista[0] == "suma":
                solucion = numeros[0]
                for lista in numeros[1:]:
                    variable = calcoohija.CalculadoraHija(solucion, lista)
                    solucion = variable.plus()
                print("La suma es igual a:")
                print(solucion)
            elif lista[0] == "resta":
                print("La resta es igual a:")
                solucion = numeros[0]
                for lista in numeros[1:]:
                    variable = calcoohija.CalculadoraHija(solucion, lista)
                    solucion = variable.minus()
                print(solucion)
            elif lista[0] == "multiplica":
                print("La multiplicacion es igual a:")
                solucion = numeros[0]
                for lista in numeros[1:]:
                    variable = calcoohija.CalculadoraHija(solucion, lista)
                    solucion = variable.multiplicacion()
                print(solucion)
            elif lista[0] == "divide":
                print("La division es igual a:")
                solucion = numeros[0]
                for lista in numeros[1:]:
                    variable = calcoohija.CalculadoraHija(solucion, lista)
                    solucion = variable.division()
                print(solucion)
