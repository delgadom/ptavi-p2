#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():
    def __init__(self, op1, op2):
        self.op1 = op1
        self.op2 = op2

    def add(self):
        return self.op1 + self.op2

    def sub(self):
        return self.op1 - self.op2

if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "+":
        result = Calculadora(operando1, operando2)
        total = result.add()
    elif sys.argv[2] == "-":
        result = Calculadora(operando1, operando2)
        total = result.sub()
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(total)
