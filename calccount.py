#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calc():

    def __init__(self):
        self.count = 0

    def add(self, op1, op2):
        self.count = self.count + 1
        return op1 + op2

    def sub(self, op1, op2):
        self.count = self.count + 1
        return op1 - op2

    def mul(self, op1, op2):
        self.count = self.count + 1
        return op1 * op2

    def div(self, op1, op2):
        try:
            total = op1/op2
            self.count = self.count + 1
        except ZeroDivisionError:

            print("Division by zero is not allowed")
            raise ValueError("Division by zero is not allowed")
        return total
