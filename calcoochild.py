#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

from calcoo import Calculadora


class CalcChild(Calculadora):
    def mul(self):
        return self.op1 * self.op2

    def div(self):
        try:
            return self.op1 / self.op2
        except ZeroDivisionError:
            raise ValueError("Division by zero is not allowed")

if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "+":
        result = Calculadora(operando1, operando2)
        total = result.add()
    elif sys.argv[2] == "-":
        result = Calculadora(operando1, operando2)
        total = result.sub()
    elif sys.argv[2] == "x":
        resulthija = CalcChild(operando1, operando2)
        total = resulthija.mul()
    elif sys.argv[2] == "/":
        try:
            resulthija = CalcChild(operando1, operando2)
            total = resulthija.div()
        except ValueError:
            ZeroDivisionError
            total = ("Division by zero is not allowed")
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(total)
